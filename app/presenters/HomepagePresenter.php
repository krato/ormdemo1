<?php

namespace App\Presenters;

use Nette;

class HomepagePresenter extends Nette\Application\UI\Presenter
{

	/** @var \App\Controls\IExampleControlFactory @inject */
	public $exampleControlFactory;

	protected function createComponentExample()
	{
		return $this->exampleControlFactory->create();
	}

}
