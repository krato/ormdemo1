-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Počítač: localhost
-- Vytvořeno: Stř 06. čec 2016, 15:46
-- Verze serveru: 5.6.31
-- Verze PHP: 7.0.7-1~dotdeb+8.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `c_ormdemo1`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `author`
--

CREATE TABLE `author` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `web` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `author`
--

INSERT INTO `author` (`id`, `name`, `web`) VALUES
(1, 'Andrew Hunt', NULL),
(2, 'Donald Knuth', NULL),
(3, 'Martin Fowler', 'http://martinfowler.com'),
(4, 'Kent Beck', NULL),
(5, 'Thomas H. Cormen', NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `reviewer_id` int(11) DEFAULT NULL,
  `pubdate` text NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `website` text,
  `available` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `book`
--

INSERT INTO `book` (`id`, `author_id`, `reviewer_id`, `pubdate`, `name`, `description`, `website`, `available`) VALUES
(1, 1, NULL, '1999-10-30', 'The Pragmatic Programmer', NULL, 'http://en.wikipedia.org/wiki/The_Pragmatic_Programmer', 1),
(2, 2, 1, '1968-04-08', 'The Art of Computer Programming', 'very old book about programming', NULL, 0),
(3, 3, 4, '1999-07-08', 'Refactoring: Improving the Design of Existing Code', NULL, 'http://martinfowler.com/books/refactoring.html', 1),
(4, 5, NULL, '2009-07-31', 'Introduction to Algorithms', 'The book covers a broad range of algorithms in depth, yet makes their design and analysis accessible to all levels of readers.', NULL, 1),
(5, 3, NULL, '2003-09-25', 'UML Distilled', NULL, 'http://martinfowler.com/books/refactoring.html', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `book_tag`
--

CREATE TABLE `book_tag` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `book_tag`
--

INSERT INTO `book_tag` (`id`, `book_id`, `tag_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 3, 2),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `borrower`
--

CREATE TABLE `borrower` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `borrower`
--

INSERT INTO `borrower` (`id`, `name`) VALUES
(1, 'Vojtech Kohout'),
(2, 'John Doe'),
(3, 'Jane Roe');

-- --------------------------------------------------------

--
-- Struktura tabulky `borrowing`
--

CREATE TABLE `borrowing` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `borrower_id` int(11) NOT NULL,
  `date` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `borrowing`
--

INSERT INTO `borrowing` (`id`, `book_id`, `borrower_id`, `date`) VALUES
(1, 1, 1, '2012-04-01'),
(2, 3, 1, '2013-01-02'),
(3, 4, 3, '2012-03-06'),
(4, 4, 3, '2012-05-06'),
(5, 1, 3, '2012-05-06');

-- --------------------------------------------------------

--
-- Struktura tabulky `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `tag`
--

INSERT INTO `tag` (`id`, `name`) VALUES
(1, 'popular'),
(2, 'ebook');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `reviewer_id` (`reviewer_id`);

--
-- Klíče pro tabulku `book_tag`
--
ALTER TABLE `book_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `book_tag_book_id_tag_id` (`book_id`,`tag_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Klíče pro tabulku `borrower`
--
ALTER TABLE `borrower`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `borrowing`
--
ALTER TABLE `borrowing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_id` (`book_id`),
  ADD KEY `borrower_id` (`borrower_id`);

--
-- Klíče pro tabulku `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `book_ibfk_2` FOREIGN KEY (`reviewer_id`) REFERENCES `author` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Omezení pro tabulku `book_tag`
--
ALTER TABLE `book_tag`
  ADD CONSTRAINT `book_tag_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `book_tag_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `borrowing`
--
ALTER TABLE `borrowing`
  ADD CONSTRAINT `borrowing_ibfk_2` FOREIGN KEY (`borrower_id`) REFERENCES `borrower` (`id`),
  ADD CONSTRAINT `borrowing_ibfk_3` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
