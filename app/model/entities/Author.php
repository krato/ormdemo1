<?php

namespace App\Model\Entities;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 *
 * @property Book[] $books m:belongsToMany
 * @property Book[] $reviewedBooks m:belongsToMany(reviewer_id)
 * @property string $web m:null
 */
class Author extends Person
{
	
}
