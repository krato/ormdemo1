<?php

namespace App\Model\Entities;

use Nette\Database\Table\ActiveRow;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
abstract class BaseEntity extends ActiveRow
{
	
}
