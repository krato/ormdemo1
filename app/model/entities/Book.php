<?php

namespace App\Model\Entities;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 *
 * @property int $id
 * @property Author $author m:hasOne
 * @property Author|null $reviewer m:hasOne(reviewer_id)
 * @property Borrowing[] $borrowings m:belongsToMany
 * @property Tag[] $tags m:hasMany
 * @property string $pubdate
 * @property string $name
 * @property string|null $description
 * @property string|null $website
 * @property bool $available
 */
class Book extends BaseEntity
{

	public function &__get($key)
	{
		switch ($key)
		{
			case 'borrowings':
				$result = $this->related('borrowing')->fetchAll();
				return $result;
				break;
			default:
				return parent::__get($key);
		}
	}

}
