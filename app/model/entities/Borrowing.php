<?php

namespace App\Model\Entities;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 *
 * @property int $id
 * @property Book $book m:hasOne
 * @property Borrower $borrower m:hasOne
 * @property string $date
 */
class Borrowing extends BaseEntity
{
	
}
