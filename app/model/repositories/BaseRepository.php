<?php

namespace App\Model\Repositories;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 * @property-read string $tableName
 */
abstract class BaseRepository extends \Nette\Object
{

	/** @var string */
	protected $tableName;

	/** @var \Nette\Database\Context */
	protected $context;

	/** @var \Nette\Caching\IStorage */
	protected $cacheStorage;

	public function __construct($tableName, \Nette\Database\Context $context, Nette\Caching\IStorage $cacheStorage = NULL)
	{
		$this->tableName = $tableName;
		$this->context = $context;
		$this->cacheStorage = $cacheStorage;
	}

	public function getTableName()
	{
		return $this->tableName;
	}

	public function findAll()
	{
		$className = $this->reflection->shortName;
		$cx = '\\App\\Model\\Selections\\' . str_replace('Repository', 'Selection', $className);
		return new $cx($this->context, $this->context->getConventions(), $this->tableName, $this->cacheStorage);
	}

}
