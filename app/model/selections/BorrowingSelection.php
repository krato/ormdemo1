<?php

namespace App\Model\Selections;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class BorrowingSelection extends BaseSelection
{

	protected function createRow(array $row)
	{
		return new \App\Model\Entities\Borrowing($row, $this);
	}

}
