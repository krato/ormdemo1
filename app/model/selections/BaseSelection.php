<?php

namespace App\Model\Selections;

use Nette\Database\Table\Selection;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
abstract class BaseSelection extends Selection
{
	
}
