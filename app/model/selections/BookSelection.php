<?php

namespace App\Model\Selections;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class BookSelection extends BaseSelection
{

	protected function createRow(array $row)
	{
		return new \App\Model\Entities\Book($row, $this);
	}

}
