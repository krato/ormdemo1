<?php

namespace App\Controls;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
interface IExampleControlFactory
{

	/** @return ExampleControl */
	function create();
}
