<?php

namespace App\Controls;

/**
 * @author Petr Kratochvil <krato@krato.cz>
 */
class ExampleControl extends \Nette\Application\UI\Component
{

	/** @var \App\Model\Repositories\BookRepository */
	protected $bookRepository;

	public function __construct(\App\Model\Repositories\BookRepository $bookRepository)
	{
		$this->bookRepository = $bookRepository;
	}

	public function render($id)
	{
		switch ($id)
		{
			case 1:
				$this->example1();
				break;
			case 2:
				$this->example2();
				break;
			case 3:
				$this->example3();
				break;
			default:
				break;
		}
	}

	function write($value, $indent = 0)
	{
		echo str_repeat(' ', $indent), $value, "\n";
	}

	function separate()
	{
		echo "\n-----\n\n";
	}

	protected function example1()
	{
		foreach ($this->bookRepository->findAll() as $book)
		{
			$this->write($book->name);
			$this->write('Autor: ' . $book->author->name);
			$this->write('Výpůjčky:');
			//dump($book);
			//dump($book->borrowings);
			foreach ($book->borrowings as $borrowing)
			{
				$this->write($borrowing->borrower->name . '(' . $borrowing->date . ')', 3);
			}
			$this->separate();
		}
	}

	protected function example2()
	{
		echo "TEST";
	}

	protected function example3()
	{
		echo "TEST";
	}

}
